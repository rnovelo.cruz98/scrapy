def func(n):
    """
    yield: Acomula todos lo valores del 
    flujo normal de ejecución del programa
    y devueleve un Generador que puede ser parseado a una lista
    o un diccionario.
    """
    for i in range(0, n):
        if (i % 2) == 0:
            yield i
    
print(list(func(10)))


def func(n):
    """
    return: devuelve el valor que quiera ser retornado y corta la ejecucion 
    inmediatamente
    """
    myList = list()
    for i in range(0, n):
        if (i % 2) == 0:
            myList.append(i)
    return myList

print(func(10))