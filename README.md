# Scrapy

## Instalación

```bash
$ pip install scrapy                                   
```

Una vez instalado scrapy podemos crear un proyecto con el comando `startproject`
```bash
$ scrapy startproject <NAME> # crear un proyecto                                    
```

El comando `startproject` genera una estructura básica de un proyecto de Scrapy

```bash
├── quotesbot
│   ├── __init__.py
│   ├── items.py
│   ├── pipelines.py # tuberias para formatear la información obtenida del sitio i.e. guardarlo en una DB
                    # o verificar que tenga cierto formato.
│   ├── settings.py # configuración del proyecto i.e.: conexión a django
│   └── spiders # spiders
│       ├── __init__.py
│       ├── toscrape-css.py # ejemplo usando busqueda por clases de css
│       └── toscrape-xpath.py # ejemplo usando busqueda por xpath (DOM)
├── LICENSE
├── README.md
└── scrapy.cfg # archivo de configuraciones
```

## Spiders

Los scripts que se encargan de hacer WebScrapping Scrapy los llama Spiders
cada uno hereda de la clase `Spider` y contiene un metodo llamado `parse`

```py
# -*- coding: utf-8 -*-
import scrapy

class MySpider(scrapy.Spider):
    # ... 
```

### yield
Es importante para scrapy que cada método de nuestro spider retorne un generador
esto se logra utilizando la instrucción `yield` de python esto le permite al framework 
poder seguir los enlances o la siguiente petición.

```py
def func(n):
    """
    yield: Acomula todos lo valores del 
    flujo normal de ejecución del programa
    y devueleve un Generador que puede ser parseado a una lista
    o un diccionario.
    """
    for i in range(0, n):
        if (i % 2) == 0:
            yield i
    
print(list(func(10)))


def func(n):
    """
    return: devuelve el valor que quiera ser retornado y corta la ejecucion 
    inmediatamente
    """
    myList = list()
    for i in range(0, n):
        if (i % 2) == 0:
            myList.append(i)
    return myList

print(func(10))
```

Esta es la una característica que diferencia a Scrapy de otras librerias de WebScrapping.

```bash
$ scrapy genspider example example.com                                         
```

Ambas extraen los mismos datos del mismo sitio web, pero `toscrape-css`
emplea selectores CSS, mientras que `toscrape-xpath` emplea expresiones XPath.

## Running the spiders

Puede ejecutar un `spider` utilizando el comando `scrapy crawl`:

    $ scrapy crawl toscrape-css

Si desea guardar los datos raspados en un archivo, puede usar la opción `-o`:
    
    $ scrapy crawl toscrape-css -o quotes.json

## Es posible ejecutar los spiders sin comandos de la siguiente forma:
Esto es util si queremos correr nuestros spiders dentro de otros scrips.

```py
import scrapy
from scrapy.crawler import CrawlerProcess

class ToScrapeCSSSpider(scrapy.Spider):
    name = "toscrape-css"
    start_urls = [
        'http://quotes.toscrape.com/',
    ]

    def parse(self, response):
        for quote in response.css("div.quote"):
            yield {
                'text': quote.css("span.text::text").extract_first(),
                'author': quote.css("small.author::text").extract_first(),
                'tags': quote.css("div.tags > a.tag::text").extract()
            }

        next_page_url = response.css("li.next > a::attr(href)").extract_first()
        if next_page_url is not None:
            yield scrapy.Request(response.urljoin(next_page_url))

process = CrawlerProcess()
process.crawl(ToScrapeCSSSpider)
process.start()
```

## Scrapyd

Es una libreria que nos permite ejecutar el servidor de scrapy y mediante CURL enviar peticiones 
para ejecutar nuestros spiders



```
$ pip install scrapyd
$ cd quotesbot
$ scrapyd
```

en otra terminal

`curl http://localhost:6800/schedule.json -d project=default -d spider=toscrape-xpath`


## Utilerias

[Pipenv](https://pipenv-fork.readthedocs.io/en/latest/basics.html)
[VSCode](https://code.visualstudio.com/Download)
[CSS Selectors vs XPath](https://medium.com/dataflow-kit/css-selectors-vs-xpath-f368b431c9dc)
[Celery](https://docs.celeryproject.org/en/latest/django/first-steps-with-django.html)
[SocketIO](https://www.botreetechnologies.com/blog/django-websocket-with-socketio)
[WebScrapping Blocking Keys](https://www.scrapehero.com/how-to-prevent-getting-blacklisted-while-scraping/)
[Robots Tag](https://seo-hacker.com/what-meta-robots-tag-are-for/)

